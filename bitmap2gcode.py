#!/usr/bin/env python3

'''
@author: tolyan
'''

import argparse
from PIL import Image
from itertools import chain
import sys

DEFAULT_POINT_SIZE = 0.15
DEFAULT_F = 3000
DEFAULT_S = 1

class Point:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

class Chain:
    def __init__(self):
        self.points = []
        
    def addPoint(self, point):
        self.points.append(point)
        
    def generateCode(self, offsetx, offsety, pointsize, F, S):
        startx = self.points[0].x * pointsize + offsetx
        starty = self.points[0].y * pointsize + offsety
        if len(self.points) == 1:
            endx = startx
            endy = starty
        else:
            endx = self.points[-1].x * pointsize + offsetx
            endy = self.points[-1].y * pointsize + offsety
        return """G0 X{0:.3f}Y{1:.3f}
M3 S{2:.0f}
G1 X{3:.3f}Y{4:.3f} F{5:.0f}
M5\n""".format(startx, starty, S, endx, endy, F)
    
    def lastPixel(self):
        return self.points[-1]

def printSize(x, y):
    s = 'Image area size: {0:.3f}x{1:.3f} mm\n'.format(x, y)
    sys.stderr.write(s)

def main():
    parser = argparse.ArgumentParser(description='Settings')
    parser.add_argument('input_image', type=str, help='Input image to process')
    parser.add_argument('-P', '--point-size', dest='pointsize', default=DEFAULT_POINT_SIZE, type=float,
                       help='Point size (default: {})'.format(DEFAULT_POINT_SIZE))
    parser.add_argument('-x', dest='dx', type=float, default=0, help='Start point offset from image center x')
    parser.add_argument('-y', dest='dy', type=float, default=0, help='Start point offset from image center y')
    parser.add_argument('-F', dest='podacha', type=int, default=DEFAULT_F, help='Podacha (default: {})'.format(DEFAULT_F))
    parser.add_argument('-S', dest='Spin', type=int, default=DEFAULT_S, help='Spin (default: {})'.format(DEFAULT_S))
    parser.add_argument('-N', '--negative', dest='negative', default=False, action='store_true', help='Generate negative image')
    parser.add_argument('-V', '--vertical', dest='vartical', default=False, action='store_true', help='vertical rasterisation')
    
    args = parser.parse_args()
    im = Image.open(args.input_image)
    
    imgsizeX, imgsizeY = im.size

    pattern = []
    pattern.append((0, 0, 0))
    pattern.append(0)
    if args.negative:
        pattern = []
        pattern.append((255, 255, 255))
        pattern.append(1)
        pattern.append(255)
    
    chains = []
    chaininterrupted = True

    if args.vartical:
        for x in range(imgsizeX - 1):
            r = range(imgsizeY - 1)
            if x % 2 == 0:
                r = reversed(r)
            for y in r:
                pixel = im.getpixel((x, y))
                if pixel in pattern:
                    if chaininterrupted:
                        chain = Chain()
                        chain.addPoint(Point(x, imgsizeY - y - 1))
                        chains.append(chain)
                        chaininterrupted = False
                    else:
                        chains[-1].addPoint(Point(x, imgsizeY - y - 1))
                else:
                    chaininterrupted = True
            chaininterrupted = True
    else:
        for y in range(imgsizeY - 1):
            r = range(imgsizeX - 1)
            if y % 2 == 0:
                r = reversed(r)
            for x in r:
                pixel = im.getpixel((x, y))
                if pixel in pattern:
                    if chaininterrupted:
                        chain = Chain()
                        chain.addPoint(Point(x, imgsizeY - y - 1))
                        chains.append(chain)
                        chaininterrupted = False
                    else:
                        chains[-1].addPoint(Point(x, imgsizeY - y - 1))
                else:
                    chaininterrupted = True
            chaininterrupted = True
                
    print("""({})
G90
G21
G49""".format(args.input_image))
    for c in chains:
        print(c.generateCode(-(imgsizeX / 2) * args.pointsize + args.dx,
                             -(imgsizeY / 2) * args.pointsize + args.dy, args.pointsize,
                             args.podacha, args.Spin))
        
    print("""M5
G0 X0 Y0 
M30
%""".format())

    printSize(imgsizeX * args.pointsize, imgsizeY * args.pointsize)
    
                        
if __name__ == '__main__':
    main()
